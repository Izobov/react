import React from "react";
import "./App.css";
import NavbarContainer from "./components/navbar/NavbarContainer";
import { Route, BrowserRouter, withRouter, Redirect } from "react-router-dom";
import UsersContainer from "./components/users/UsersContainer";
import HeaderContainer from "./components/header/HeaderContainer";
import {Login} from "./components/login/login";
import { connect, Provider } from "react-redux";
import { compose } from "redux";
import { initialazeApp } from "./redux/app_reducer";
import store, { AppStateType } from "./redux/redux-store";
import Preloader from "./components/common/preloader/Preloader";
import { withSuspense } from "./hoc/WithSupsense";
import News from "./components/News/News";
import Music from "./components/Music/Music";
import Settings from "./components/Settings/Settings";

const DialogsContainer = React.lazy(() =>
  import("./components/Dialogs/DialogsContainer")
);
const ProfileContainer = React.lazy(() =>
  import("./components/profile/ProfileContainer")
);

const SuspenedDialogs= withSuspense(DialogsContainer)
const SuspenedProfile= withSuspense(ProfileContainer)

type PropsType={
  initialazeApp:()=>void,
  initialazed: boolean
}

class App extends React.Component<PropsType> {
  catchAllUnhandledErrors = (promiseReject: PromiseRejectionEvent) => {
    alert("Some Error");
    console.log(promiseReject);
  };

  componentDidMount() {
    this.props.initialazeApp();
    window.addEventListener("unhandledrejection", this.catchAllUnhandledErrors);
  }
  componentWillUnmount() {
    window.removeEventListener(
      "unhandledrejection",
      this.catchAllUnhandledErrors
    );
  }

  render() {
    if (!this.props.initialazed) {
      return <Preloader/>  }

    return (
      <div className="app-wrapper">
        <HeaderContainer />
        <NavbarContainer />
        <div className="app-wrapper-content">
          <Route exact path="/" render={() => <Redirect to={"/profile"} />} />
          <Route path="/dialogs" render={()=><SuspenedDialogs/>} />

          <Route
            path="/profile/:userId?"
            render={()=><SuspenedProfile/>}
          />
          <Route path="/news" render={() => <News />} />
          <Route path="/music" render={() => <Music />} />
          <Route path="/settings" render={() => <Settings />} />
          <Route path="/users" render={() => <UsersContainer pageTitle={"Samyrai"} />} />
          <Route path="/login" render={() => <Login />} />
          {/* <Route path="*" render={() => <div>404 not found</div>} /> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state:AppStateType) => ({
  initialazed: state.app.initialazed
});

const AppContainer= compose<React.ComponentType>(
  withRouter, 
  connect(mapStateToProps,{initialazeApp}))(App)

const ReactJSApp:React.FC=()=>{
  return <BrowserRouter>
  <Provider store={store}>
  <AppContainer/>
  </Provider>
  </BrowserRouter>
}

export default ReactJSApp 
