import { usersAPI } from "../api/usersAPI";
import { UserType } from "../types/types";
import {  InferActionsTypes, BaseThunkType } from "./redux-store";




let initialState = {
  users: [] as Array<UserType>,
  pageSize: 10,
  totalUsersCount: 0,
  currentPage: 1,
  isFetching: false,
  followingInProgress: [] as Array<number>, // array of users ids
  filter:{
    term: '', 
    friend: null as null | boolean
  }
};



const users_reducer = (state = initialState, action: ActionsTypes): InitialStateType => {
  switch (action.type) {
    case "FOLLOW":
      return {
        ...state,
        users: state.users.map((u) => {
          if (u.id === action.userID) {
            return { ...u, followed: true };
          }
          return u;
        }),
      };

    case "UNFOLLOW":
      return {
        ...state,
        users: state.users.map((u) => {
          if (u.id === action.userID) {
            return { ...u, followed: false };
          }
          return u;
        }),
      };

    case "SET_USERS": {
      return { ...state, users: action.users };
    }

    case "SET_CURENT_PAGE": {
      return { ...state, currentPage: action.currentPage };
    }

    case "SET_TOTAL_USERS_COUNT": {
      return { ...state, totalUsersCount: action.totalUsersCount };
    }

    case "TOGGLE_IS_FETCHING":
      return {
        ...state,
        isFetching: action.isFetching,
      };
    case "FOLLOWING_IN_PROGRESS":
      return {
        ...state,
        followingInProgress: action.isFetching
          ? [...state.followingInProgress, action.userID]
          : state.followingInProgress.filter((id) => id !== action.userID),
      };

    case "SET_FILTER":
      return {
        ...state,
        filter:action.payload
      }

    default:
      return state;
  }
};


export const actions ={

  follow:(userID: number) => ({
    type: "FOLLOW",
    userID,
  }as const),
  unfollow: (userID: number)=> ({
    type: "UNFOLLOW",
    userID,
  }as const),
  setUsers: (users: Array<UserType>) => ({
    type: "SET_USERS",
    users,
  }as const),
  setCurrentPage: (currentPage: number) => ({
    type: "SET_CURENT_PAGE",
    currentPage,
  }as const),
  setTotalUsersCount: (
    totalUsersCount: number
  ) => ({
    type: "SET_TOTAL_USERS_COUNT",
    totalUsersCount,
  }as const),
  toggleIsFetching: (
    isFetching: boolean
  ) => ({
    type: "TOGGLE_IS_FETCHING",
    isFetching,
  }as const),
  followingIsFetching: (
    isFetching: boolean,
    userID: number
  ) => ({
    type: "FOLLOWING_IN_PROGRESS",
    isFetching,
    userID,
  }as const),
  setFilter:(filter:FilterType)=>({type:"SET_FILTER", payload:filter} as const)
}


export const getUsers = (currentPage: number, pageSize: number, filter:FilterType):ThunkType => async (
  dispatch
) => {
  dispatch(actions.toggleIsFetching(true));
  let data = await usersAPI.getUsers(currentPage, pageSize, filter.term, filter.friend);
  dispatch(actions.toggleIsFetching(false));
  dispatch(actions.setUsers(data.items));
  dispatch(actions.setTotalUsersCount(data.totalCount));
  dispatch(actions.setFilter(filter));
};

export const unfollowThunk = (userID: number):ThunkType => async (dispatch) => {
  dispatch(actions.followingIsFetching(true, userID));
  let data = await usersAPI.unfollow(userID);

  if (data.resultCode === 0) {
    dispatch(actions.unfollow(userID));
  }
  dispatch(actions.followingIsFetching(false, userID));
};

export const followThunk = (userID: number):ThunkType => async (dispatch) => {
  dispatch(actions.followingIsFetching(true, userID));
  let data = await usersAPI.follow(userID);

  if (data.resultCode === 0) {
    dispatch(actions.follow(userID));
  }
  dispatch(actions.followingIsFetching(false, userID));
};

export default users_reducer;

export type InitialStateType = typeof initialState;
export type FilterType= typeof initialState.filter;
type ActionsTypes=InferActionsTypes<typeof actions>
type ThunkType =BaseThunkType<ActionsTypes>
