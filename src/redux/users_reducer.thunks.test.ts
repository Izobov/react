import { followThunk, actions, unfollowThunk } from "./users_reducer"
import { usersAPI } from "../api/usersAPI"
import { ApiResponseType, ResultCodesEnum } from "../api/api";
jest.mock("../api/usersAPI")
const userAPIMock=usersAPI as jest.Mocked<typeof usersAPI>;
const result:ApiResponseType={
  resultCode: ResultCodesEnum.Success,
  messages:[],
  data:{}
}


userAPIMock.follow.mockReturnValue(Promise.resolve(result))
userAPIMock.unfollow.mockReturnValue(Promise.resolve(result))

const dispatchMock= jest.fn()
const getStateMock= jest.fn();
beforeEach(()=>{
  dispatchMock.mockClear();
  getStateMock.mockClear();
  userAPIMock.follow.mockClear();
  userAPIMock.unfollow.mockClear()
})


test("follow thunk success",()=>{

  const thunk= followThunk(1)


  thunk(dispatchMock, getStateMock, {})

  expect(dispatchMock).toBeCalledTimes(1)
  expect(dispatchMock).toHaveBeenNthCalledWith(1, actions.followingIsFetching(true, 1)) // говорим что должно диспатчиться

})
test("unfollow thunk success",()=>{

  const thunk= unfollowThunk (1)

  thunk(dispatchMock, getStateMock, {})

  expect(dispatchMock).toBeCalledTimes(1)
  expect(dispatchMock).toHaveBeenNthCalledWith(1, actions.followingIsFetching(true, 1)) // говорим что должно диспатчиться

})