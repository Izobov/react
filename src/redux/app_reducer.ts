import { setAuthUser } from "./auth_reducer";
import { ThunkAction } from "redux-thunk";
import { AppStateType, InferActionsTypes } from "./redux-store";

let initialState = {
  initialazed: false,
};
export type InitialStateType= typeof initialState
type ActionsType=InferActionsTypes<typeof actions>

const app_reducer = (state = initialState, action: ActionsType): InitialStateType => {
  switch (action.type) {
    case "APP/INITIALAZED_SUCCESS":
      return {
        ...state,
        initialazed: true,
      };

    default:
      return state;
  }
};


export const actions={
  initialazedSuccess: () => ({
    type: "APP/INITIALAZED_SUCCESS",
  }as const)

}


type ThunkType= ThunkAction<void, AppStateType, unknown, ActionsType>

export const initialazeApp = ():ThunkType => (dispatch) => {
  let promise = dispatch(setAuthUser());

  Promise.all([promise]).then(() => {
    dispatch(actions.initialazedSuccess());
  });
};

export default app_reducer;
