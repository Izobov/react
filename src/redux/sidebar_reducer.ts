export type Friend = {
  id: number;
  name: string;
  ava: string;
};

let initialState = {
  friends: [
    {
      id: 1,
      name: "Vlad",
      ava:
        "https://o-viber.ru/wp-content/uploads/2017/08/mujskie_avatarki-5.png",
    },
    {
      id: 2,
      name: "Tatyana",
      ava:
        "https://i.pinimg.com/originals/6f/61/5e/6f615e73e5c86f89001b973d1a68c321.jpg",
    },
    {
      id: 3,
      name: "Nikolay",
      ava: "https://wxpcdn.gcdn.co/dcont/fb/image/crew4_1024.png",
    },
    { id: 4, name: "Dima", ava: "http://img.bibo.kz/11962/11962201.JPG" },
    { id: 5, name: "Olga", ava: "https://vibirai.ru/image/964470.jpg" },
  ] as Array<Friend>,
};

type InitialState = typeof initialState;

const sidebar_reducer = (state = initialState, action: any): InitialState => {
  switch (action.type) {
    default:
      return state;
  }
};
export default sidebar_reducer;
