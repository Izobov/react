import users_reducer, { InitialStateType, actions } from "./users_reducer";


let state:InitialStateType;
beforeEach(()=>{

  state={
    users: [
      {id:1, name:"Ivan 1", followed:false, photos:{small:null, large:null}, status:"some 1"},
      {id:2, name:"Ivan 2", followed:false, photos:{small:null, large:null}, status:"some 2"},
      {id:3, name:"Ivan 3", followed:true, photos:{small:null, large:null}, status:"some 3"},
      {id:4, name:"Ivan 4", followed:true, photos:{small:null, large:null}, status:"some 4"},
      
    ],
    pageSize: 10,
    totalUsersCount: 0,
    currentPage: 1,
    isFetching: false,
    followingInProgress: []
  };
})
test("follow success",()=>{ 
  const newState= users_reducer(state, actions.follow(2))
  expect(newState.users[0].followed).toBeFalsy()
  expect(newState.users[1].followed).toBeTruthy()
})


test("unfollow success",()=>{
  const newState= users_reducer(state, actions.unfollow(3))
  expect(newState.users[2].followed).toBeFalsy()
  expect(newState.users[3].followed).toBeTruthy()
})