import profile_reducer from "./profile_reducer";
import dialog_reducer from "./dialog_reducer";
import sidebar_reducer from "./sidebar_reducer";


export let store = {

    _state: {
        profilePage: {
            posts: [
                { id: 1, post: 'Hi, how are you?', likes: 15 },
                { id: 2, post: "It's my first post", likes: 10 },
            ],

            newPostText: ''
        },

        dialogsPage: {
            dialogsData: [
                { id: 1, name: 'Vlad', ava: 'https://o-viber.ru/wp-content/uploads/2017/08/mujskie_avatarki-5.png' },
                { id: 2, name: 'Tatyana', ava: 'https://i.pinimg.com/originals/6f/61/5e/6f615e73e5c86f89001b973d1a68c321.jpg' },
                { id: 3, name: 'Nikolay', ava: 'https://wxpcdn.gcdn.co/dcont/fb/image/crew4_1024.png' },
                { id: 4, name: 'Dima', ava: 'http://img.bibo.kz/11962/11962201.JPG' },
                { id: 5, name: 'Olga', ava: 'https://vibirai.ru/image/964470.jpg' }
            ],
            messagesData: [
                { id: 1, massage: "Здарова" },
                { id: 2, massage: "Дурачек" },
                { id: 3, massage: "Че делаешь?" },
            ],

            message: ''

        },

        aside: {
            friends: [
                { id: 1, name: 'Vlad', ava: 'https://o-viber.ru/wp-content/uploads/2017/08/mujskie_avatarki-5.png' },
                { id: 2, name: 'Tatyana', ava: 'https://i.pinimg.com/originals/6f/61/5e/6f615e73e5c86f89001b973d1a68c321.jpg' },
                { id: 3, name: 'Nikolay', ava: 'https://wxpcdn.gcdn.co/dcont/fb/image/crew4_1024.png' },
                { id: 4, name: 'Dima', ava: 'http://img.bibo.kz/11962/11962201.JPG' },
                { id: 5, name: 'Olga', ava: 'https://vibirai.ru/image/964470.jpg' }
            ],
        }
    },

    getState() {
        return this._state;
    },

    _callSubscriber() {
        console.log('state changed')
    },

    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {
        this._state.profilePage = profile_reducer(this._state.profilePage, action);
        this._state.dialogsPage = dialog_reducer(this._state.dialogsPage, action);
        this._state.aside = sidebar_reducer(this._state.aside, action);

        this._callSubscriber(this._state);
    }


}





export default store;
window.store = store;