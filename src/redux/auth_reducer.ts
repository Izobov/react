import { ResultCodesEnum, ResultCodeForCaptchaEnum } from "../api/api";
import {authAPI} from "../api/authAPI"
import {securityAPI} from "../api/securityAPI"
import { stopSubmit, FormAction } from "redux-form";
import {  InferActionsTypes, BaseThunkType } from "./redux-store";


let initialState = {
  userId: null as null | number,
  email: null as null | string,
  login: null as null | string,
  isAuth: false,
  isFetching: false,
  captchaUrl: null as null | string,
};

const auth_reducer = (state = initialState, action: ActionsType): InitialStateType => {
  switch (action.type) {
    case "SET_USER_DATA":

    case "GET_CAPTCHA_URL_SUCCESS":
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};






export const actions = {

  setAuthUserData: (
    userId: number | null,
    email: string | null,
    login: string | null,
    isAuth: boolean
  ) => ({
    type: "SET_USER_DATA",
    payload: { userId, email, login, isAuth },
  }as const),
  
  getCaptchaUrlSuccess: (
    captchaUrl: string
  ) => ({
    type: "GET_CAPTCHA_URL_SUCCESS",
    payload: { captchaUrl },
  }as const)
}



export const loginThunk = (
  email: string,
  password: string,
  rememberMe: boolean,
  captcha: string
):ThunkType => async (dispatch) => {
  let data = await authAPI.login(email, password, rememberMe, captcha);
  if (data.resultCode === ResultCodesEnum.Success) {
    dispatch(setAuthUser());
  } else {
    if (data.resultCode ===ResultCodeForCaptchaEnum.CaptchaIsRequired) {
      dispatch(getCaptchaUrl());
    }

    let message =
      data.messages.length > 0 ? data.messages[0] : "Some Errore";
    let action = stopSubmit("login", { _error: message });
    dispatch(action);
  }
};

export const logoutThunk = ():ThunkType => async (dispatch) => {
  let res = await authAPI.logout();
  if (res.data.resultCode === 0) {
    dispatch(actions.setAuthUserData(null, null, null, false));
  } else if (res.data.resultCode === 10) {
  } else {
    let message = res.data.message.length > 0 ? res.data.message : "some error";
    dispatch(stopSubmit("login", { _error: message }));
  }
};

export const setAuthUser = ():ThunkType => async (dispatch) => {
  let res = await authAPI.authMe();
  
  if (res.resultCode === ResultCodesEnum.Success) {
    let { id, login, email } = res.data;

    dispatch(actions.setAuthUserData(id, login, email, true));
  }
};

export const getCaptchaUrl = ():ThunkType => async (dispatch) => {
  const data = await securityAPI.getCaptchaUrl();
  const captchaUrl =data.url;
  dispatch(actions.getCaptchaUrlSuccess(captchaUrl));
};

export default auth_reducer;

export type InitialStateType = typeof initialState;
type ActionsType = InferActionsTypes<typeof actions>
type ThunkType= BaseThunkType<ActionsType|FormAction>