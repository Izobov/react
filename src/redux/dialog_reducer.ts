import { InferActionsTypes } from "./redux-store";





export type DialogType = {
  id: number;
  name: string;
  ava: string;
};
export type MessageType = {
  id: number
  message: string
};

let initialState = {
  dialogsData: [
    {
      id: 1,
      name: "Vlad",
      ava:
        "https://o-viber.ru/wp-content/uploads/2017/08/mujskie_avatarki-5.png",
    },
    {
      id: 2,
      name: "Tatyana",
      ava:
        "https://i.pinimg.com/originals/6f/61/5e/6f615e73e5c86f89001b973d1a68c321.jpg",
    },
    {
      id: 3,
      name: "Nikolay",
      ava: "https://wxpcdn.gcdn.co/dcont/fb/image/crew4_1024.png",
    },
    { id: 4, name: "Dima", ava: "http://img.bibo.kz/11962/11962201.JPG" },
    { id: 5, name: "Olga", ava: "https://vibirai.ru/image/964470.jpg" },
  ] as Array<DialogType>,
  messagesData: [
    { id: 1, message: "Здарова" },
    { id: 2, message: "Дурачек" },
    { id: 3, message: "Че делаешь?" },
  ] as Array<MessageType>,
};

const dialog_reducer = (state = initialState, action:ActionsType):InitialStateType => {
  switch (action.type) {
    case "ADD_MESSAGE":
      return {
        ...state,
        messagesData: [
          ...state.messagesData,
          { id: state.messagesData.length+1, message: action.message},
        ] as Array<MessageType>,
      };
    default:
      return state;
  }
};

export const actions={
  addMessageActionCreator:(message:string)=> ({
    type: "ADD_MESSAGE",
    message
  }as const)
}


export default dialog_reducer;

export type InitialStateType = typeof initialState;
type ActionsType= InferActionsTypes<typeof actions>