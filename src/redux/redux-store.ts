import { createStore, combineReducers, applyMiddleware, Action } from 'redux';
import profile_reducer from './profile_reducer';
import dialog_reducer from './dialog_reducer';
import sidebar_reducer from './sidebar_reducer';
import users_reducer from './users_reducer';
import auth_reducer from './auth_reducer';
import thunkMidleware, {ThunkAction}from 'redux-thunk';
import { reducer as formReducer } from 'redux-form'
import app_reducer from './app_reducer';



export type InferActionsTypes<T> =  T extends {[key:string]:(...args: any[])=>infer U}? U:never

export type BaseThunkType <A extends Action, R=Promise<void>>= ThunkAction<R, AppStateType, unknown, A>

let rootReducer = combineReducers({
    profilePage: profile_reducer,
    dialogsPage: dialog_reducer,
    aside: sidebar_reducer,
    usersPage: users_reducer,
    auth: auth_reducer,
    form: formReducer,
    app: app_reducer,

});

type RootReducerType= typeof rootReducer
export type AppStateType=ReturnType<RootReducerType>

let store = createStore(rootReducer, applyMiddleware(thunkMidleware));

export default store
//@ts-ignore
window.store = store;