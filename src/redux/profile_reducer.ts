import { profileAPI } from "../api/profileAPI";
import { PostType, ProfileType, PhotosType } from "../types/types";
import { stopSubmit, FormAction } from "redux-form";
import {  InferActionsTypes, BaseThunkType } from "./redux-store";



let initialState = {
  posts: [
    { id: 1, post: "Hi, how are you?", likes: 15 },
    { id: 2, post: "It's my first post", likes: 10 },
  ] as Array<PostType>,

  profile: null as ProfileType | null,
  status: "",
};



const profile_reducer = (
  state = initialState,
  action: ActionsTypes
): InitialStateType => {
  switch (action.type) {
    case "ADD_POST": {
      let newPost = {
        id: 3,
        post: action.text,
        likes: 0,
      };
      return {
        ...state,
        posts: [...state.posts, newPost],
      };
    }

    case "SET_USER_PROFILE":
      return {
        ...state,
        profile: action.profile,
      };

    case "SET_SATUS":
      return {
        ...state,
        status: action.status,
      };

    case "DELETE_POST":
      return {
        ...state,
        posts: state.posts.filter((i) => i.id !== action.id),
      };
    case "SAVE_PHOTO_SUCCESS":
      return {
        ...state,
        profile: { ...state.profile, photos: action.photos } as ProfileType,
      };
    default:
      return state;
  }
};


export const actions={
  addPostActionCreator : (
    text: string
  ) => ({ type: "ADD_POST", text }as const),
  
  deletePost :(id: number)=> ({
    type: "DELETE_POST",
    id,
  }as const),

  setUserProfile: (profile: ProfileType) => ({
    type: "SET_USER_PROFILE",
    profile,
  }as const ),
  savePhotoSuccess: (photos: PhotosType) => ({
    type: "SAVE_PHOTO_SUCCESS",
    photos,
  }as const ),
  
  setStatus: (status: string) => ({
    type: "SET_SATUS",
    status,
  }as const ),
}



export const getStatus = (userID: number):ThunkType => async (dispatch) => {
  let data = await profileAPI.getStatus(userID);
  dispatch(actions.setStatus(data));
};

export const updateStatus = (status: string):ThunkType => async (dispatch) => {
  let data = await profileAPI.updateStatus(status);

  if (data.resultCode === 0) {
    dispatch(actions.setStatus(status));
  }
};

export const savePhoto = (file: File):ThunkType => async (dispatch) => {
  let data = await profileAPI.savePhoto(file);

  if (data.resultCode === 0) {
    dispatch(actions.savePhotoSuccess(data.data.photos));
  }
};

export const saveProfile = (profile: ProfileType):ThunkType => async (
  dispatch,
  getState
) => {
  const userId = getState().auth.userId;

  let res = await profileAPI.saveProfile(profile);

  if (res.data.resultCode === 0) {
    if(userId!==null){
      dispatch(setUserProfileThunk(userId)); 
    }else{
      throw new Error("userId can't be null")
    }
  } else {
    dispatch(stopSubmit("edit-profile", { _error: res.data.messages[0] }));
  }
};

export const setUserProfileThunk = (userID: number|null):ThunkType => async (
  dispatch
) => {
  let data = await profileAPI.userProfile(userID);

  dispatch(actions.setUserProfile(data));
};

export default profile_reducer;
export type InitialStateType = typeof initialState;
type ActionsTypes = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsTypes|FormAction>
