import profileReducer, {
  actions,
} from "./profile_reducer";

let state = {
  posts: [
    { id: 1, post: "Hi, how are you?", likes: 15 },
    { id: 2, post: "It's my first post", likes: 10 },
  ],
  profile: null,
  status: "",
};

it("shoud deletePost", () => {
  let action = actions.deletePost(1);
  let newState = profileReducer(state, action);

  expect(newState.posts.length).toBe(1);
});

it("shoud add new post", () => {
  let action = actions.addPostActionCreator("hello, it is test");
  let newState = profileReducer(state, action);

  expect(newState.posts.length).toBe(3);
});
