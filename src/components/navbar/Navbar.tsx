import React from "react";
import s from "./Navbar.module.css";
import { NavLink } from "react-router-dom";
import Friends from "./Friends/Friends";
import { Friend } from "../../redux/sidebar_reducer";

type PropsType={
  friends:Array<Friend>
}

const Navbar: React.FC<PropsType> = (props) => {
  let friends = props.friends.map((friends) => (
    <Friends
      key={friends.id}
      name={friends.name}
      id={friends.id}
      ava={friends.ava}
    />
  ));

  return (
    <nav className={s.nav}>
      <div className={s.item}>
        <NavLink activeClassName={s.active} to={"/profile"}>
          Profile
        </NavLink>
      </div>
      <div className={`${s.item} ${s.active}`}>
        <NavLink activeClassName={s.active} to="/dialogs">
          Messages
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink activeClassName={s.active} to="/news">
          News
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink activeClassName={s.active} to="/music">
          Music
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink activeClassName={s.active} to="/settings">
          Settings
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink activeClassName={s.active} to="/users">
          Users
        </NavLink>
      </div>
      <div className={s.friends}>
        FRIENDS
        <div className={s.friends_items}>{friends.splice(0, 3)}</div>
      </div>
    </nav>
  );
};

export default Navbar;
