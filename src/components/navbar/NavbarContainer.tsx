import React from "react";
// import s from './Navbar.module.css';
// import { NavLink } from 'react-router-dom';
// import Friends from './Friends/Friends';
import Navbar from "./Navbar";
import { connect } from "react-redux";
import { AppStateType } from "../../redux/redux-store";
type MapPropsType= ReturnType<typeof mapStateToProps>

let NavbarContainer: React.FC<MapPropsType> = (props) => {
  return <Navbar {...props} />;
};

let mapStateToProps = (state:AppStateType) => {
  return {
    myId: state.auth.userId,
    friends: state.aside.friends,
  };
};

export default NavbarContainer = connect<MapPropsType, {},{},AppStateType>(mapStateToProps, {})(Navbar);
