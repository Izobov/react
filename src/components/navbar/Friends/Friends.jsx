import React from 'react';
import s from './Friends.module.css';



const Friends = (props) => {

    return <div>
        <div className={s.item}>
            <img src={props.ava} alt="" />
            {props.name}
        </div>
    </div>
}

export default Friends;