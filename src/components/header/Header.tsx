import React from 'react';
import s from './Header.module.css';
import { NavLink } from 'react-router-dom'
import logo from '../../img/logo/logo.png';
export type MapPropsType={
    isAuth:boolean
    login:string|null
}
export type DispatchPropsType={
    logoutThunk: ()=>void
}

const Header:React.FC<MapPropsType&DispatchPropsType> = (props) => {
    return <header className={s.header}>
        <div className={s.logo}>
            <img src={logo} />
        </div>

        <h1>Traveller</h1>

        <div className={s.loginBlock}>
            {props.isAuth
                ? <div className={s.loginWrapper}>
                    <div className={s.login}>
                        {props.login}
                    </div >
                    <div className={s.loginButton} onClick={props.logoutThunk}>
                        Logout
                    </div>
                </div>
                : <NavLink to={'/login'}>
                    <div className={s.loginButton}>

                        Login
                    </div>
                </NavLink>}

        </div>
    </header>
}

export default Header;