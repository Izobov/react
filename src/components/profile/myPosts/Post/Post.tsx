import React from 'react';
import s from './Post.module.css';

type PropsType={
    message: string
    likes: number
}
const Post: React.FC<PropsType> = (props) => {
    return <div className={s.item}>
        <img src='https://hornews.com/images/news_large/c1d4b2b8ec608ea72764c5678816d5c9.jpg' />
        {props.message}
        <div>
            {props.likes}
            <span>like</span>
        </div>
    </div>

}

export default Post;