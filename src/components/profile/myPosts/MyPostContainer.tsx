import React from 'react';
import {actions} from '../../../redux/profile_reducer';
import MyPosts, { MapPropsType, DispatchPropsType } from './MyPost';
import { connect } from 'react-redux';
import { AppStateType } from '../../../redux/redux-store';



const mapStateToProps = (state: AppStateType) => {
    return {
        posts: state.profilePage.posts,
    }
}


const MyPostsContainer = connect<MapPropsType, DispatchPropsType,{},AppStateType>(mapStateToProps, {addPost: actions.addPostActionCreator})(MyPosts);

export default MyPostsContainer;