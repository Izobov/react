import React, { Props } from 'react';
import { Field, reduxForm, InjectedFormProps  } from 'redux-form';
import { required, maxLengthCreator } from '../../../helpers/validators/validator';
import { Textarea, createField, GetStringKeys } from '../../common/formsControls/formsControls';

const maxLength = maxLengthCreator(10);
type PropsType={

}
export type AddPostFormValuesType={
    postText:string
}
type AddPostFormValuesKeys =GetStringKeys<AddPostFormValuesType>

const PostForm: React.FC<InjectedFormProps<AddPostFormValuesType, PropsType>& PropsType>= (props) => {


    return <form onSubmit={props.handleSubmit}>
        <div>
        {createField<AddPostFormValuesKeys>("post message", 'postText', [required, maxLength], Textarea)}
        </div>

        <button >add post</button>

    </form>
}

const PostReduxForm = reduxForm<AddPostFormValuesType,PropsType>({
    form: 'post',
})(PostForm);

export default PostReduxForm