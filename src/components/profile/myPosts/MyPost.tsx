import React from "react";
import s from "./MyPost.module.css";
import Post from "./Post/Post";
import PostReduxForm, { AddPostFormValuesType } from "./PostsForm";
import { PostType } from "../../../types/types";

export type MapPropsType={
  posts: Array<PostType>
}
export type DispatchPropsType={
  addPost: (postText:string)=> void
}

const MyPosts: React.FC<MapPropsType&DispatchPropsType> =(props) => {
  // shouldComponentUpdate(nextProps, nextState) {
  //     return nextProps != this.props || nextState != this.state

  // эта логика вмонтирована в PureComponent

  // }

  let postsElements = props.posts.map((post) => (
    <Post message={post.post} key={post.id} likes={post.likes} />
  ));

  let onSubmit = (postData: AddPostFormValuesType) => {
    props.addPost(postData.postText);
  };

  return (
    <div className={s.posts_block}>
      <h3>My posts</h3>
      <PostReduxForm onSubmit={onSubmit} />
      <div className={s.post}>
        {postsElements}
        {/* <Post message={postsData[0].post} likes={postsData[0].likes} />
            <Post message={postsData[1].post} likes={postsData[1].likes} /> */}
      </div>
    </div>
  );
}

const MyPostsMemo = React.memo(MyPosts);

export default MyPostsMemo;
