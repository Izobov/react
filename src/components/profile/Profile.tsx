import React from "react";
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import MyPostsContainer from "./myPosts/MyPostContainer";
import { ProfileType } from "../../types/types";
type PropsType={
  profile: ProfileType|null,
  status: string,
  isOwner: boolean,
  saveProfile: (profile:ProfileType)=>Promise<any>,
  updateStatus:(status:string)=>void,
  savePhoto: (file:File)=>void,
}
const Profile: React.FC<PropsType> = (props) => {
  return (
    <div>
      <ProfileInfo
        profile={props.profile}
        status={props.status}
        updateStatus={props.updateStatus}
        isOwner={props.isOwner}
        savePhoto={props.savePhoto}
        saveProfile={props.saveProfile}
      />
      <MyPostsContainer />
    </div>
  );
};

export default Profile;
