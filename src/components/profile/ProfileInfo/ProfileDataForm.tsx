import React from "react";
import {
  createField,
  Input,
  Textarea,
  GetStringKeys,
} from "../../common/formsControls/formsControls";
import { reduxForm, InjectedFormProps } from "redux-form";
import s from "./ProfileInfo.module.css";
import e from "../../common/formsControls/formsControls.module.css";
import { ProfileType } from "../../../types/types";

type PropsType={
  profile:ProfileType
}
type ProfileTypeKeys=GetStringKeys<ProfileType>

const ProfileDataForm:React.FC<InjectedFormProps<ProfileType, PropsType>& PropsType> = ({ handleSubmit, profile, error }) => {
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <div>
          <button>save</button>
          {error && <div className={e.formSummaryError}>{error}</div>}
        </div>
        <div>
          <b>Full name:</b>
          {createField<ProfileTypeKeys>("Full Name", "fullName", [], Input)}
        </div>
        <div>
          <b>About me:</b>
          {createField<ProfileTypeKeys>("About me", "aboutMe", [], Textarea)}
        </div>
        <div>
          <b>Looking for a job: </b>
          {createField<ProfileTypeKeys>("", "lookingForAJob", [], Input, {
            type: "checkbox",
          })}
        </div>
        <div>
          <b>Job Description:</b>
          {createField<ProfileTypeKeys>("My skills", "lookingForAJobDescription", [], Textarea)}
        </div>
        <div>
          <b>Contacts:</b>
          {Object.keys(profile.contacts).map((key) => {
            return (
              <div className={s.contact} key={key}>
                <b>{key}: </b>
                {createField(key, "contacts." + key, [], Input)}
              </div>
            );
          })}
        </div>
      </div>
    </form>
  );
};

const ProfileDataFormReduxForm = reduxForm<ProfileType, PropsType>({
  form: "edit-profile",
})(ProfileDataForm);

export default ProfileDataFormReduxForm;
