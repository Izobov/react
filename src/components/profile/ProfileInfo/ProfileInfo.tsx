import React, { useState, ChangeEvent } from "react";
import user from "../../../img/users/users.jpg";
import no from "../../../img/yesNo/cross.png";
import yes from "../../../img/yesNo/yes.png";
import Preloader from "../../common/preloader/Preloader";
import s from "./ProfileInfo.module.css";
import ProfileStatusWithHooks from "./ProfileStatusWithHooks";
import ProfileDataForm from "./ProfileDataForm";
import { ProfileType, ContactsType } from "../../../types/types";

type PropsType={
  profile: ProfileType|null,
  status: string,
  isOwner: boolean,
  saveProfile: (profile:ProfileType)=>Promise<any>,
  updateStatus:(status:string)=>void,
  savePhoto: (file:File)=>void,
}


const ProfileInfo: React.FC<PropsType> = ({
  profile,
  status,
  isOwner,
  saveProfile,
  savePhoto,
  updateStatus,
}) => {
  const onPhotoSelected = (e:ChangeEvent<HTMLInputElement>) => {
    if (e.target.files&& e.target.files.length) {
      savePhoto(e.target.files[0]);
    }
  };

  const onSubmit = (formData: ProfileType) => {
    saveProfile(formData).then((res) => {
      if (res) setEditMode(false);
    });
  };

  const [editMode, setEditMode] = useState(false);

  if (!profile) {
    return <Preloader />;
  }
  return (
    <div>
      <div className={s.description_Block}>
        <img src={profile.photos.large || user} alt="" />
        {isOwner && <input type="file" onChange={onPhotoSelected} />}
        <div className={s.info}>
          <h2>{profile.fullName}</h2>
          <ProfileStatusWithHooks status={status} updateStatus={updateStatus} />
          {editMode ? (
            <ProfileDataForm
              initialValues={profile}
              profile={profile}
              onSubmit={onSubmit}
            />
          ) : (
            <ProfileData
              profile={profile}
              isOwner={isOwner}
              setEditMode={setEditMode}
            />
          )}
        </div>
      </div>
    </div>
  );
};

type PofileDataProps={
  profile:ProfileType,
  isOwner:boolean,
  setEditMode:(bool:boolean)=>void
}

const ProfileData: React.FC<PofileDataProps> = ({ profile, isOwner, setEditMode }) => {
  return (
    <div>
      {isOwner && (
        <div>
          <button onClick={() => setEditMode(true)}>edit</button>
        </div>
      )}
      <div>
        <b>About me:</b>
        {profile.aboutMe}
      </div>
      <div>
        <b>Looking for a job: </b>
        <img src={profile.lookingForAJob ? yes : no} />
      </div>
      <div>
        <b>Job Description:</b>
        {profile.lookingForAJobDescription}
      </div>
      <div>
        <b>Contacts:</b>
        {Object.keys(profile.contacts).map((key) => {
          return (
            <Contact
              key={key}
              contactTitle={key}
              contactValue={profile.contacts[key as keyof ContactsType]}
            />
          );
        })}
      </div>
    </div>
  );
};

type ContactsPropsType={
  contactTitle:string,
  contactValue: string 
}

const Contact: React.FC<ContactsPropsType> = ({ contactTitle, contactValue }) => {
  return (
    <div className={s.contact}>
      <b>{contactTitle}:</b>
      <a href={contactValue} target="_blank">
        {contactValue}
      </a>
    </div>
  );
};

export default ProfileInfo;
