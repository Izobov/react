import React, { useState, useEffect, ChangeEvent } from "react";
import s from "./ProfileInfo.module.css";
type PropsType={
  status: string
  updateStatus: (status: string)=>void
}
const ProfileStatusWithHooks: React.FC<PropsType> = (props) => {
  // let stateWithSetState = useState(true);
  // let editeMode = stateWithSetState[0];
  // let setEditeMode= stateWithSetState[1];

  let [editeMode, setEditeMode] = useState(false);
  let [status, setStatus] = useState(props.status);

  useEffect(() => {
    setStatus(props.status);
  }, [props.status]);

  let activatedMode = () => {
    setEditeMode(true);
  };

  let diactivatedEditeMode = () => {
    setEditeMode(false);
    props.updateStatus(status);
  };

  let onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
    setStatus(e.currentTarget.value);
  };

  return (
    <div>
      {!editeMode && (
        <div>
          <span onDoubleClick={activatedMode}>
            {props.status || "no status"}
          </span>
        </div>
      )}
      {editeMode && (
        <div>
          <input
            type="text"
            autoFocus={true}
            onBlur={diactivatedEditeMode}
            onChange={onStatusChange}
            value={status}
          />
        </div>
      )}
    </div>
  );
};

export default ProfileStatusWithHooks;
