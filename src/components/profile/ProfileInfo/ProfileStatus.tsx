import React, { useEffect, useState, ChangeEvent } from "react";
import s from "./ProfileInfo.module.css";

type PropsType={
  status: string
  updateStatus:(status: string)=>void
}
type StateType={
  editMode:boolean
  status: string
}

class ProfileStatus extends React.Component <PropsType, StateType>{
  state = {
    editMode: false,
    status: this.props.status,
  };

  activatedEditMode = () => {
    this.setState({
      editMode: true,
    });
  };

  diactvatedEditMode = () => {
    this.setState({
      editMode: false,
    });
    this.props.updateStatus(this.state.status);
  };

  onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      status: e.currentTarget.value,
    });
  };

  componentDidUpdate = (prevProps: PropsType, prevState: StateType) => {
    if (prevProps.status !== this.props.status) {
      this.setState({
        status: this.props.status,
      });
    }
  };

  render() {
    return (
      <div>
        {!this.state.editMode && (
          <div>
            <span onDoubleClick={this.activatedEditMode}>
              {this.props.status || "no status"}
            </span>
          </div>
        )}
        {this.state.editMode && (
          <div>
            <input
              type="text"
              onChange={this.onStatusChange}
              value={this.state.status}
              onBlur={this.diactvatedEditMode}
              autoFocus={true}
            />
          </div>
        )}
      </div>
    );
  }
}

export default ProfileStatus;
