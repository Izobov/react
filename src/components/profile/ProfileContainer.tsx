import React from "react";
import { connect } from "react-redux";
import Profile from "./Profile";
import {
  setUserProfileThunk,
  getStatus,
  updateStatus,
  savePhoto,
  saveProfile,
} from "../../redux/profile_reducer";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { withAuthRedirect } from "../../hoc/WithAuthRedirect";
import { compose } from "redux";
import { AppStateType } from "../../redux/redux-store";
import { ProfileType } from "../../types/types";
type MapPropsType= ReturnType<typeof mapStateToProps>
type MapDispatchType={
  setUserProfileThunk: (userId:number)=>void,
  getStatus: (userId:number)=>void,
  updateStatus: (status:string)=>void,
  savePhoto: (file: File)=>void,
  saveProfile: (profile: ProfileType)=>Promise<any>,
}
type PathParamsType={
  userId:string
}

type PropsType=MapPropsType&MapDispatchType&RouteComponentProps<PathParamsType>

class ProfileContainer extends React.Component<PropsType> {
  refreshProfile() {
    let userID:number|null = +this.props.match.params.userId;
    if (!userID) {
      userID = this.props.myId;
      if (!userID) {
        return <Redirect to={"/login"} />;
      }
    }

    this.props.setUserProfileThunk(userID);
    this.props.getStatus(userID);
  }

  componentDidMount() {
    this.refreshProfile();
  }

  componentDidUpdate(prevProps:PropsType, prevState: PropsType) {
    if (this.props.match.params.userId !== prevProps.match.params.userId) {
      this.refreshProfile();
    }
  }

  render() {
    return (
      <Profile
        {...this.props}
        updateStatus={this.props.updateStatus}
        isOwner={!this.props.match.params.userId}
        savePhoto={this.props.savePhoto}
      />
    );
  }
}

let mapStateToProps = (state:AppStateType) => ({
  profile: state.profilePage.profile,
  status: state.profilePage.status,
  isAuth: state.auth.isAuth,
  myId: state.auth.userId,
});
export default compose<React.ComponentType>(
  connect(mapStateToProps, {
    setUserProfileThunk,
    getStatus,
    updateStatus,
    savePhoto,
    saveProfile,
  }),
  withRouter,
  withAuthRedirect
)(ProfileContainer);
