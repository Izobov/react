import React from "react";
import s from "./formsControls.module.css";
import { Field, WrappedFieldProps, WrappedFieldMetaProps } from "redux-form";
import { FieldValidatorType } from "../../../helpers/validators/validator";


type FormControlParamsType ={
  meta: WrappedFieldMetaProps
}



export const FormControl:React.FC<FormControlParamsType>= ({ meta: { touched, error }, children }) => {
  const hasError = touched && error;
  return (
    <div className={s.formControl + " " + (hasError ? s.error : " ")}>
      {children}
      {hasError && <span>{error}</span>}
    </div>
  );
};



export const Textarea: React.FC<WrappedFieldProps> = (props) => {
  const { input, meta,  ...restProps } = props;
  return (
    <FormControl {...props}>
      <textarea {...props.input} {...restProps} />
    </FormControl>
  );
};

export const Input: React.FC<WrappedFieldProps> = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={s.formControl + " " + (hasError ? s.error : " ")}>
      <input {...input} {...props} />
      {hasError && <span>{meta.error}</span>}
    </div>
  );
};

// type LoginFormValuesTypeKeys =keyof LoginFormDataType // создает перечисление один из вариантов

export function createField <FormKeysType extends string> ( //ожидаем какие-то данные в <FormKeysType>
  placeholder:string|undefined,
  name: FormKeysType,                       // подставляем сюда
  validators:Array<FieldValidatorType>,
  component:React.FC<WrappedFieldProps>,
  props = {},
  text = ""
){
  return <div>
    <Field
      placeholder={placeholder}
      component={component}
      name={name}
      validate={validators}
      {...props}
    />{" "}
    {text}
  </div>
};

export type GetStringKeys<T> =Extract<keyof T, string>