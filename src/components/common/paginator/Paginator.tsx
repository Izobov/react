import React, { useState } from "react";
import styles from "./styles.module.css";
import rightArrow from "../../../img/rightArrow.png";
import leftArrow from "../../../img/leftArrow.png";
import cn from "classnames";

type PropsType ={
  totalUsersCount:number,
  pageSize:number, 
  currentPage:number,
  onPageChanged: (PageNumber:number)=>void
}


let Paginator:React.FC<PropsType> = ({ totalUsersCount, pageSize, currentPage, onPageChanged }) => {
  let pagesCount = Math.ceil(totalUsersCount / pageSize);

  let pages:Array<number> = [];
  let [portionNum, setPortionNum] = useState(1);
  let portionSize = 10;
  let portionCount = Math.ceil(pagesCount / portionSize);
  let leftPageNum = (portionNum - 1) * portionSize + 1;
  let rightPageNum = portionNum * portionSize;

  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }

  return (
    <div className={styles.pages}>
      {portionNum !== 1 && (
        <div
          className={styles.arrow}
          onClick={() => setPortionNum(portionNum - 1)}
        >
          <img src={leftArrow} alt="" />
        </div>
      )}
      {pages
        .filter((p) => p >= leftPageNum && p <= rightPageNum)
        .map((p) => {
          return (
            <span
              key={p}
              onClick={() => {
                onPageChanged(p);
              }}
              className={cn(styles.page, {
                [styles.selectPage]: currentPage === p,
              })}
            >
              {p}
            </span>
          );
        })}
      {portionNum !== portionCount && (
        <div
          className={styles.arrow}
          onClick={() => setPortionNum(portionNum + 1)}
        >
          <img src={rightArrow} alt="" />
        </div>
      )}
    </div>
  );
};

export default Paginator;
