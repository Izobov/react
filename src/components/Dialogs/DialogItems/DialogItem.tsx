import React from 'react';
import s from './../Dialogs.module.css';
import { NavLink } from 'react-router-dom';
import { DialogType } from '../../../redux/dialog_reducer';


const DialogsItem: React.FC<DialogType> = (props) => {
    return <div className={`${s.dialog} ${s.active}`}>
        <NavLink to={"/dialogs/" + props.id}>
            <div className={s.ava}>
                <img src={props.ava} alt="" />
                {props.name}
            </div>
        </NavLink>
    </div>
}

export default DialogsItem;