import React from 'react';
import { Field, reduxForm,  InjectedFormProps } from 'redux-form';
import { Textarea, createField, GetStringKeys } from '../common/formsControls/formsControls';
import { required, maxLengthCreator } from '../../helpers/validators/validator';
import { NewMessageFormType } from './Dialogs';

const maxLength = maxLengthCreator(20);
type NewMessageFormValuesTypeKeys =GetStringKeys <NewMessageFormType>
type PropsType={}


const DialogForm : React.FC<InjectedFormProps<NewMessageFormType, PropsType>& PropsType>= (props) => {

return <form onSubmit={props.handleSubmit}>
        {createField<NewMessageFormValuesTypeKeys>("Enter message", 'message', [required, maxLength], Textarea)}
       
        <button>send message</button>
    </form>
}

const DialogReduxForm = reduxForm<NewMessageFormType, PropsType>({
    form: 'message'
})(DialogForm)

export default DialogReduxForm;