import React from "react";
import s from "./Dialogs.module.css";
import DialogsItem from "./DialogItems/DialogItem";
import Message from "./Message/Message";
import DialogReduxForm from "./DialogsForm";
import { InitialStateType } from "../../redux/dialog_reducer";

type PropsType={
  dialogsPage:InitialStateType
  addMessageActionCreator:(message:string)=> void
}

export type NewMessageFormType={
  message:string
}


const Dialogs:React.FC<PropsType> = (props) => {
  let state=props.dialogsPage
  let dialogsElements = state.dialogsData.map((d) => (
    <DialogsItem name={d.name} key={d.id} ava={d.ava} id={d.id} />
  ));

  let messagesElements = state.messagesData.map((m) => (
    <Message message={m.message} key={m.id} />
  ));

  let onSubmit = (messageData:NewMessageFormType) => {
    props.addMessageActionCreator(messageData.message);
    
  };

  return (
    <div className={s.dialogs}>
      <div className={s.dialogsItems}>{dialogsElements}</div>
      <div className={s.messages}>
        <div className={s.messagesElements}>{messagesElements}</div>

        <DialogReduxForm onSubmit={onSubmit}  />
      </div>
    </div>
  );
};

export default Dialogs;
