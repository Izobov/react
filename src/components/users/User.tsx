import React from "react";
import styles from "./styles.module.css";
import userPhoto from "../../img/users/users.jpg";
import { NavLink } from "react-router-dom";
import { UserType } from "../../types/types";

type PropsType={
  user:UserType
  followingInProgress: Array<number>
  unfollow: (userId:number)=>void
  follow:(userId:number)=>void
}

let User: React.FC<PropsType> = ({ user, followingInProgress, unfollow, follow }) => {
  return (
    <div className={styles.usersWrapper}>
      <div className={styles.photoWrapper}>
        <div>
          <NavLink to={"/profile/" + user.id}>
            <img
              src={user.photos.small != null ? user.photos.small : userPhoto}
              className={styles.ava}
              alt=""
            />
          </NavLink>
        </div>
        <div>
          {user.followed ? (
            <button
              disabled={followingInProgress.some((id) => id === user.id)}
              onClick={() => {
                unfollow(user.id);
              }}
            >
              Unfollow
            </button>
          ) : (
            <button
              disabled={followingInProgress.some((id) => id === user.id)}
              onClick={() => {
                follow(user.id);
              }}
            >
              Follow
            </button>
          )}
        </div>
      </div>
      <div className={styles.infoWrapper}>
        <div className={styles.userName}>
          <NavLink to={"/profile/" + user.id}>{user.name}</NavLink>
        </div>
        <div className={styles.userStatus}>{user.status}</div>
      </div>
    </div>
  );
};

export default User;
