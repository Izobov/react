import React, { useEffect } from "react";
import Paginator from "../common/paginator/Paginator";
import User from "./User";
import UsersSearchForm from "./UsersSearchForm";
import { actions, FilterType, followThunk, getUsers } from "../../redux/users_reducer";
import { useDispatch, useSelector } from "react-redux";
import { getCurrentPage, getFollowingInProgress, getIsFetching, getPageSize, getTotalUsersCount, getUsersFilter, getUsersSelector } from "../../redux/selectors/user_selectors";
import Preloader from "../common/preloader/Preloader";
import { useHistory } from "react-router-dom";
import * as queryString from 'query-string'


type PropsType = {
  

}

export const Users: React.FC<PropsType> = (props) => {

  const totalUsersCount = useSelector(getTotalUsersCount);
  const currentPage = useSelector(getCurrentPage);
  const pageSize = useSelector(getPageSize);
  const users = useSelector(getUsersSelector);
  const followingInProgress = useSelector(getFollowingInProgress)
  const filter = useSelector(getUsersFilter);
  const isFetching =useSelector(getIsFetching);
  
  const dispatch = useDispatch();
  const history = useHistory();
  
  useEffect(()=>{

    const {search} = history.location;
    const parsed = queryString.parse(search);
    const actualPage = parsed.page ? +parsed.page : currentPage;
    let actualFilter =  filter;
    if (parsed.term)  actualFilter = {...actualFilter, term: parsed.term as string} ;
    if (parsed.frriend)  actualFilter = {...actualFilter, friend: parsed.friend === "null" ? null : parsed.friend === "true" ? true: false} ;
    dispatch(actions.setCurrentPage(actualPage))
    dispatch( getUsers(actualPage, pageSize, actualFilter))
    return () => {
      dispatch(actions.setCurrentPage(1));
      onFilterChanged({term:"", friend: null})
    }
  },[]);

  useEffect(() => {
    history.push({
      pathname: "/users",
      search: `?term=${filter.term}&friend=${filter.friend}&page=${currentPage}`
    })
  }, [filter, currentPage])

  const onPageChanged = (pageNumber: number) => {
    dispatch(actions.setCurrentPage(pageNumber))
    dispatch( getUsers(pageNumber, pageSize, filter))
  }

  const onFilterChanged = (filter:FilterType) => {
    dispatch(actions.setCurrentPage(1));    
    dispatch( getUsers(1, pageSize, filter))
  };
  const follow = (id:number)=>{
    dispatch(followThunk(id))
  };
  const unfollow  = (id:number)=>{
    dispatch(followThunk(id))
  };
  if (isFetching) {
    return <Preloader/>
  }
  return (
     <div>
      <UsersSearchForm onFilterChanged={onFilterChanged}/>
      {users.map((u) => (
        <User
          key={u.id}
          user={u}
          followingInProgress={followingInProgress}
          follow={follow}
          unfollow={unfollow}
        />
      ))}
      <Paginator
        totalUsersCount={totalUsersCount}
        pageSize={pageSize}
        currentPage={currentPage}
        onPageChanged={onPageChanged}
      />
    </div>
  );
};




