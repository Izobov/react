import React from "react";

import {Formik, Form, Field} from 'formik'
import { FilterType } from "../../redux/users_reducer";
import { useSelector } from "react-redux";
import { getUsersFilter } from "../../redux/selectors/user_selectors";

const formValidate = (values:any) => {
    const errors = {};
    return errors;
}
type FriendType =  "true"|"false"|"null"
type FormType={
    term:string
    friend: FriendType
}
  
type PropsType={
    onFilterChanged:(filter:FilterType)=>void
}

const UsersSearchForm: React.FC<PropsType> =React.memo( (props)=>{
  const filter = useSelector(getUsersFilter)
    const submit=(values:FormType, { setSubmitting }:{setSubmitting:(isSubmitting:boolean)=>void}) => {
      
      
        props.onFilterChanged(filter);
        setSubmitting(false)
   }
    
    return <div>
    <Formik
          enableReinitialize
          initialValues={{ term: filter.term, friend:`${filter.friend }` as FriendType}}
          validate={formValidate}
          onSubmit={submit}
        >
          {({ isSubmitting }) => (
            
            <Form>
              <Field type="text" name="term" />
              <Field name="friend" as="select">
                <option value="null">All </option>
                <option value="true">Only friends</option>
                <option value="false">Not friends</option>
              </Field>
              <button type="submit" disabled={isSubmitting}>
                Submit
              </button>
            </Form>
          )}
        </Formik>
     </div>
})

export default UsersSearchForm