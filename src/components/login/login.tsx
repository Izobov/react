import React from "react";
import { reduxForm, InjectedFormProps } from "redux-form";
import { connect, useDispatch, useSelector } from "react-redux";
import { loginThunk } from "../../redux/auth_reducer";
import { Input, createField, GetStringKeys } from "../common/formsControls/formsControls";
import { required } from "../../helpers/validators/validator";
import { Redirect } from "react-router-dom";
import s from "../common/formsControls/formsControls.module.css";
import i from "./login.module.css";
import { AppStateType } from "../../redux/redux-store";

type LoginFormOwnPropsType= {
  captchaUrl:string | null
}

const LoginForm: React.FC<InjectedFormProps<LoginFormDataType, LoginFormOwnPropsType>& LoginFormOwnPropsType>= ({ handleSubmit, error, captchaUrl }) => {
  return (
    <form onSubmit={handleSubmit}>
      {createField<LoginFormValuesTypeKeys>("Email", 'email', [required], Input)}
      {createField<LoginFormValuesTypeKeys>("Password", "password", [required], Input, {
        type: "password",
      })}
      {createField<LoginFormValuesTypeKeys>(
        undefined,
        "rememberMe",
        [],
        Input,
        { type: "checkbox" },
        "remeber me"
      )}
      {captchaUrl && <img src={captchaUrl} className={i.img} />}
      {captchaUrl && createField<LoginFormValuesTypeKeys>("Enter symbols", "captcha", [required], Input)}
      <div>
        {error && <div className={s.formSummaryError}>{error}</div>}

        <button>Login</button>
      </div>
    </form>
  );
};

const LoginReduxForm = reduxForm<LoginFormDataType, LoginFormOwnPropsType>({
  form: "login",
})(LoginForm);





type LoginFormDataType={
  email:string,
  password:string,
  rememberMe:boolean,
  captcha:string
}
type LoginFormValuesTypeKeys =GetStringKeys<LoginFormDataType>


export const Login: React.FC = (props) => {
  const  captchaUrl = useSelector((state: AppStateType) => state.auth.captchaUrl)
  const isAuth = useSelector ((state:AppStateType)=>state.auth.isAuth)
  const dispatch = useDispatch()
  
  const onSubmit = (formData:LoginFormDataType) => {
      dispatch(loginThunk(
        formData.email,
        formData.password,
        formData.rememberMe,
        formData.captcha
      ));
    };

    if (isAuth) {
      return <Redirect to={`/profile`} />;
    }
    return (
      <div>
        <h1>Login</h1>
        <LoginReduxForm onSubmit={onSubmit} captchaUrl={captchaUrl} />
      </div>
    );
    }
 



