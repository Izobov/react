import  axios, { AxiosResponse } from "axios";
import {instance, ApiResponseType} from "./api"
import { ProfileType, PhotosType } from "../types/types";

type SavePhotoResDataType={
  photos:PhotosType
}

export const profileAPI = {
  userProfile(userID:number|null) {
    return instance.get<ProfileType>(`profile/${userID}`).then(res=>res.data);
  },

  getStatus(userID:number) {
    return instance.get<string>(`profile/status/` + userID).then(res=> res.data);
  },

  updateStatus(status:string) {
    return instance.put<ApiResponseType>(`profile/status`, { status: status }).then(res=>res.data);
  },
  savePhoto(file:any) {
    const photo = new FormData();
    photo.append("image", file);
    return instance.put<ApiResponseType<SavePhotoResDataType>>(`profile/photo`, photo, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    }).then(res=> res.data);
  },
  saveProfile(profile:ProfileType) {
    return instance.put<ApiResponseType<ProfileType>>(`profile`, profile);
  },
};
