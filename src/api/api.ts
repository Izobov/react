import  axios, { AxiosResponse } from "axios";
import { ProfileType, UserType } from "../types/types";

export const instance = axios.create({
  withCredentials: true,
  baseURL: `https://social-network.samuraijs.com/api/1.0/`,
  headers: {
    "API-KEY": "cc8b2eef-627b-427e-91cd-b8c48644bd2b",
  },
});


export enum ResultCodesEnum{
  Success=0,
  Error = 1,
  
}
export enum ResultCodeForCaptchaEnum{
  CaptchaIsRequired=10
}

export type GetItemsType ={
  items: Array<UserType>
  totalCount:number
  error:string | null
}
export type ApiResponseType<D={}, RC=ResultCodesEnum>={
  data:D
  messages: Array<string>
  resultCode: RC
}