import  axios, { AxiosResponse, AxiosPromise } from "axios";
import {instance, GetItemsType,ApiResponseType} from "./api"


export const usersAPI = {
    getUsers(currentPage:number=1, pageSize:number=10, term="", friend: null|boolean=null) {
      return instance
        .get<GetItemsType>(`users?page=${currentPage}&count=${pageSize}&term=${term}`+(friend===null? '':`&friend=${friend}`))
        .then((response) => {
          return response.data;
        });
    },
  
    follow(userID:number) {
      return instance.post<ApiResponseType>(`follow/${userID}`).then((response) => {
        return response.data;
      });
    },
  
    unfollow(userID:number) {
      return instance.delete(`follow/${userID}`).then((response) => {
        return response.data;
      }) as Promise<ApiResponseType>;
    },
  
};