import  axios, { AxiosResponse } from "axios";
import {instance, ResultCodesEnum, ResultCodeForCaptchaEnum,ApiResponseType} from "./api"




type AuthResponseType = {
  id:number,
  email:string, 
  login: string
 
}
type LoginResponseType = {
  userId:number 
}

export const authAPI = {
  authMe() {
    return instance.get<ApiResponseType<AuthResponseType>>(`auth/me`).then((response) => {
      return response.data;
    });
  },

  login(email:string, password:string, rememberMe:boolean = false, captcha:string|null = null) {
    return instance.post<ApiResponseType<LoginResponseType, ResultCodeForCaptchaEnum | ResultCodesEnum>>(`auth/login`, {
      email,
      password,
      rememberMe,
      captcha,
    }).then(res=> res.data);
  },

  logout() {
    return instance.delete(`auth/login`).then(res=>res.data);
  },
};