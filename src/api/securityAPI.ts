import  axios, { AxiosResponse } from "axios";
import {instance} from "./api"
import { type } from "os";

type CaptchaUrlResType={
  url: string
}

export const securityAPI = {
  getCaptchaUrl() {
    return instance.get<CaptchaUrlResType>(`security/get-captcha-url`).then(res=>res.data);
  },
};

